package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Gmtn {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Gmtn() {
    }
    
    public ArrayList<Song> getGmtnSongs() {
    	
    	 //Artist and tracks are all stylized in lowercase
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("furioso melodia", "gmtn.");                    //Create songs to populate album
         Song track2 = new Song("trappola bewitching", "gmtn.");                //Continue creating songs
         this.albumTracks.add(track1);                                          //Add the first song to song list for gmtn.
         this.albumTracks.add(track2);                                          //Continue adding songs to song list
         return albumTracks;                                                    //Return the songs for gmtn. in the form of an ArrayList
    }

}
