package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class ManWithAMission {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public ManWithAMission() {
    }
    
    public ArrayList<Song> getMWAMSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Dog Days", "Man with a Mission");              //Create songs to populate album
         Song track2 = new Song("Get Off My Way", "Man with a Mission");        //Continue creating songs
         Song track3 = new Song("Distance", "Man with a Mission");              //Continue creating songs
         Song track4 = new Song("evils fall", "Man with a Mission");            //This is how the song title is stylized, not a mistake
         Song track5 = new Song("Dead End in Tokyo", "Man with a Mission");     //Continue creating songs
         this.albumTracks.add(track1);                                          //Add the first song to song list for Man with a Mission
         this.albumTracks.add(track2);                                          //Continue adding songs to song list
         this.albumTracks.add(track3);                                          //Continue adding songs to song list
         this.albumTracks.add(track4);                                          //Continue adding songs to song list
         this.albumTracks.add(track5);                                          //Continue adding songs to song list
         return albumTracks;                                                    //Return the songs for Man with a Mission in the form of an ArrayList
    }

}
